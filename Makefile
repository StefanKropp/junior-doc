all:
	$(MAKE) -C quickguide $@

%:
	$(MAKE) -C quickguide $@
	$(MAKE) -C scripts $@

.PHONY: all
